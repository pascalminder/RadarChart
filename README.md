# RadarChart.js

Copyright (c) 2013 Pascal Minder, pascal.minder[at]trustus.ch

## Notes

RadarChart.js is a simple HTML5 Chart class using the canvas element.

## Usages

Include the RadarChart.js into your html file.

```html
<script src="RadarChart.js"></script>

<canvas id="environmentMap" width="800" height="600"></canvas>
```

Create a new RadarChart.js object with the right parameters.

```javascript
var map;

// Create a new RadarChart and initialize it with the canvas element.
map = new RadarChart(document.getElementById('environmentMap').getContext('2d'));

// Set the default values.
mapObject.init({MinAngle: 0, MaxAngle: 180, AngleScale: 10, ValueScale: 10, ValueScaleByMaxMin: false, PointColor: "rgba(151,187,205,1)", PointStrokeColor: "#fff", ValueScaleMax: 80, ValueScaleMin: 0});

// Add a new datapoint to the chart.
mapObject.addPoint(value, angle, objectTemperature, ambientTemperature);

// Update the chart.
mapObject.drawDiagram();
```


## License

RadarChart.js is available under the MIT license.